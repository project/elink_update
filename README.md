CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Maintainers

INTRODUCTION
------------

This module finds the external URLs from within the body field content and updates those with target and rel (optional) attribute respectively.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/elink_update

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/search/elink_update


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


USAGE
------

1. Using configuration form
   * Go to "/admin/config/elink-update".
   * Select the required options.
   * Submit the form.

2. Using Drush
   * If you don't have drush, then you can use the configuration form mentioned #1.
   * Use the below Drush help command to get a list of available options:
     * "drush help elink-update:find-external-link"
   * Example - 'drush fel article _blank --rel=nofollow,noreferrer'
     Here -
       * 'fel' - drush command alias.
       * 'article' - the content type machine name.
       * '_blank' - the target attribute value to be set.
       * 'rel' - contains the values to be set against rel attribute.

MAINTAINERS
-------------

Current maintainers:
* Swapnil Bijwe (vb_swapnil) - https://www.drupal.org/u/vb_swapnil
* Renuka Kulkarni (renukakulkarni) - https://www.drupal.org/u/renukakulkarni
