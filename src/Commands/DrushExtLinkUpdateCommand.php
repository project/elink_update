<?php

namespace Drupal\elink_update\Commands;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 */
class DrushExtLinkUpdateCommand extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Entity type service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * A Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get('elink_update');
  }

  /**
   * Find external links and add link & relation attributes to it.
   *
   * @param string $node_type
   *   Type of the node to update.
   * @param string $link_target
   *   Target link attribute to be set.
   *   [default:_blank, Allowed values: _blank|_self|_parent|_top].
   * @param array $options
   *   Array of options as described below.
   *
   * @command elink-update:find-external-link
   * @aliases fel
   * @pluginId content
   * @validate-module-enabled node
   *
   * @option rel A comma-separated list of rel attribute values to add.
   *   [Allowed values: nofollow, noreferrer, noopener, external]
   *
   * @usage "fel article _blank --rel=nofollow,noreferrer"
   *   Update external links.
   */
  public function findExternalLink($node_type = '', $link_target = '', array $options = ['rel' => self::OPT]) : void {
    $result = $this->validateInputs($node_type, $link_target, $options);
    if ($result['status'] == FALSE) {
      $this->logger->error(dt($result['error']));
    }
    else {
      try {
        $query = $this->entityTypeManager->getStorage('node')->getQuery();
        $nids = $query->condition('type', $node_type)->condition('status', '1')->execute();
      }
      catch (\Exception $e) {
        $this->logger->error(dt('Unable to proceed due to exception: @e', ['@e' => $e]));
      }

      if (!empty($nids)) {
        try {
          $batchBuilder = (new BatchBuilder())
            ->setFinishCallback('finished_processing');
          $this->output()->writeln(dt('Starting batch process to find external links'));
          if (isset($options['rel'])) {
            $options['rel'] = array_map('trim', explode(',', $options['rel']));
          }
          foreach ($nids as $nid) {
            $batchBuilder->addOperation('process_node', [
              $nid,
              $link_target,
              $options,
            ]);
          }
          batch_set($batchBuilder->toArray());
          drush_backend_batch_process();
          $this->output()->writeln(dt('Batch operation has ended and command executed successfully.'));
        }
        catch (\Exception $e) {
          $this->logger->error(dt('Unable to proceed due to exception: @e', ['@e' => $e]));
        }
      }
      else {
        $this->logger->error(dt('No nodes of type: @type found.', ['@type' => $node_type]));
      }
    }

  }

  /**
   * Validate inputs.
   *
   * @param string $node_type
   *   Node type.
   * @param string $link_target
   *   Link target.
   * @param array $options
   *   Options.
   *
   * @return array
   *   Returns validate results.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateInputs($node_type = '', $link_target = '', array $options = []) {
    // Validate node type.
    if (empty($node_type)) {
      return [
        'status' => FALSE,
        'error' => 'Missing argument "Node type".',
      ];
    }
    else {
      // Check if the node type provided exist within the site.
      $all_available_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
      if (!array_key_exists($node_type, $all_available_types)) {
        return [
          'status' => FALSE,
          'error' => 'Content type entered does not exist on this site.',
        ];
      }
    }

    // Validate target attribute.
    if (empty($link_target)) {
      return [
        'status' => FALSE,
        'error' => 'Missing argument "Link target".',
      ];
    }
    else {
      // Check if the provided target value is valid.
      $allowed_target_values = ['_blank', '_self', '_parent', '_top'];
      if (!in_array($link_target, $allowed_target_values)) {
        return [
          'status' => FALSE,
          'error' => 'Invalid target value.',
        ];
      }
    }

    // Validate link rel attribute.
    if (!empty($options['rel'])) {
      $allowed_rel_values = ['nofollow', 'noreferrer', 'noopener', 'external'];
      $rel_options = array_map('trim', explode(',', $options['rel']));
      if (!empty(array_diff($rel_options, $allowed_rel_values))) {
        $allowed_values = implode(', ', $allowed_rel_values);
        return [
          'status' => FALSE,
          'error' => "Invalid rel attribute value. Provide a comma separate list of these allowed values: $allowed_values.",
        ];
      }
    }
    return [
      'status' => TRUE,
    ];
  }

}
