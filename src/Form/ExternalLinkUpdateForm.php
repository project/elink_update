<?php

namespace Drupal\elink_update\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * External Link Update Form.
 */
class ExternalLinkUpdateForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Entity type service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * A Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get('elink_update');
  }

  /**
   * Function create.
   *
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'external_link_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['link_update_content_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select Content Type'),
      '#required' => TRUE,
      '#options' => $this->getContentTypes(),
    ];

    $form['link_target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Link Target'),
      '#options' => [
        '_blank' => $this->t('_blank'),
        '_self' => $this->t('_self'),
        '_parent' => $this->t('_parent'),
        '_top' => $this->t('_top'),
      ],
      '#required' => TRUE,
      '#size' => 1,
    ];
    $form['link_rel_attribute'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select rel Attribute'),
      '#options' => [
        'nofollow' => $this->t('nofollow'),
        'noreferrer' => $this->t('noreferrer'),
        'noopener' => $this->t('noopener'),
        'external' => $this->t('external'),
      ],
      '#size' => 1,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_types = array_filter($form_state->getValue('link_update_content_type'));
    $link_target = $form_state->getValue('link_target_type');
    $options['rel'] = $form_state->getValue('link_rel_attribute');

    foreach ($content_types as $content_type) {
      try {
        $query = $this->entityTypeManager->getStorage('node')->getQuery();
        $nids = $query->condition('type', $content_type)->condition('status', '1')->execute();
      }
      catch (\Exception $e) {
        $this->logger->error('Unable to proceed due to exception: @e', ['@e' => $e]);
      }
      if (!empty($nids)) {
        try {
          $batchBuilder = new BatchBuilder();
          $batchBuilder->setTitle($this->t('Processing Batch...'))
            ->setFinishCallback('finished_processing')
            ->setInitMessage($this->t('Initializing Batch...'))
            ->setProgressMessage($this->t('Completed @current of @total. Estimated remaining time: @estimate. Elapsed time : @elapsed.'))
            ->setErrorMessage($this->t('An error has occurred.'));
          foreach ($nids as $nid) {
            $batchBuilder->addOperation('process_node', [
              $nid,
              $link_target,
              $options,
            ]);
          }
          batch_set($batchBuilder->toArray());
          $this->messenger()->addMessage('Batch operation has ended and update is completed successfully.');
        }
        catch (\Exception $e) {
          $this->logger->error('Unable to proceed due to exception: @e', ['@e' => $e]);
        }
      }
      else {
        $this->logger->error('No nodes of type: @type found.', ['@type' => $content_type]);
        return;
      }
    }

  }

  /**
   * Get list of content types.
   */
  private function getContentTypes() {
    $types = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $types[$contentType->id()] = $contentType->label();
    }
    return $types;
  }

}
